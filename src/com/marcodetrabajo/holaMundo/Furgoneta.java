package com.marcodetrabajo.holaMundo;

public class Furgoneta extends Auto {
	
	private int carga;
	private int volumen;
	
	
	
	public Furgoneta(String matricula, String marca, String modelo, String color, float tarifa,boolean disponibilidad,int carga,int volumen) {
		super(matricula, marca, modelo, color, tarifa,disponibilidad);
		
		this.carga=carga;
		this.volumen=volumen;
	}



	public int getCarga() {
		return this.carga;
	}
	public int getVolumen() {
		return this.volumen;
	}
	
	
	public String getAtributos() {
		return super.getAtributos()+
		"  carga: "+getCarga()+" "+
		"  volumen: "+getVolumen();
	}

	
}
