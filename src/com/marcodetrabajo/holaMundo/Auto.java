package com.marcodetrabajo.holaMundo;

public abstract class Auto {
	
	
	//definimos los atributos de la clase
	
	private String matricula;
	private String marca;
	private String modelo;
	private String color;
	private float tarifa;
	private boolean disponibilidad;
	
	// definimos el contructor

	public Auto(String matricula,String marca,String modelo,String color,float tarifa,boolean disponibilidad){
	
	this.matricula=matricula;
	this.marca=marca;
	this.modelo=modelo;
	this.color=color;
	this.tarifa=tarifa;
	this.disponibilidad=disponibilidad;
	
	}
	
	//getters y setters
	
		public String getmatricula() {
			return this.matricula;
		}
		public String getmarca() {
			return this.marca;
		}
		public String getmodelo() {
			return this.modelo;
		}
		public String getcolor() {
			return this.color;
		}
		public float gettarifa() {
			return this.tarifa;
		}
		public boolean getdisponibilidad() {
			return this.disponibilidad;
		}
		
		
		public void settarifa(float tarifa) {
			this.tarifa=tarifa;
		}
		public void setdisponibilidad(boolean disponibilidad) {
			this.disponibilidad=disponibilidad;
		}
		
		
		
		
		public String getAtributos() {
			
			return 
			" Matricula: "+getmatricula()+" "+
			" Modelo : "+getmodelo()+" "+
			" color: "+getcolor()+" "+
			" tarifa: "+gettarifa()+" "+
			" disponibilidad: "+getdisponibilidad();
		}
		

		
}

