package com.marcodetrabajo.holaMundo;

public class Turismo extends Auto {
	
	//declaracion atributos propios de TURISMO
	private int numPuertas;
	private boolean marchaAutomatica;
	

	
	//metodo constructor
	public Turismo(String matricula, String marca, String modelo, String color, float tarifa,boolean disponibilidad,int numPuertas,boolean marchaAutomatica) {
		super(matricula, marca, modelo, color, tarifa,disponibilidad);
		
		this.numPuertas=numPuertas;
		this.marchaAutomatica=marchaAutomatica;
		
	}



	public int getNumPuertas() {
		return numPuertas;
	}

	public boolean getMarchaAutomatica() {
		return marchaAutomatica;
	}


		public String getAtributos() {
			return super.getAtributos()+
			"  numero de puertas: "+getNumPuertas()+" "+
			"marcha automatica: "+getMarchaAutomatica()+" ";
				
		}


}

