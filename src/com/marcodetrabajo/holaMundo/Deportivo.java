package com.marcodetrabajo.holaMundo;

public class Deportivo extends Auto {

	
	private int cilindrada;
	
	
	
	//constructor
	
	public Deportivo(String matricula, String marca, String modelo, String color, float tarifa,boolean disponibilidad,int cilindrada) {
		super(matricula, marca, modelo, color, tarifa,disponibilidad);


		this.cilindrada=cilindrada;
	}
	
	public int getCilindrada() {
		return this.cilindrada;
	}
	
	public String getAtributos() {
		return super.getAtributos()+
		"  cilindrada: "+getCilindrada();
	}
	

}
