package com.marcodetrabajo.holaMundo;

public class AutoAlquilado {
	
	/*public static void main(String args[]) {
		
		Auto auto1=new Auto("S13DW","citroen","c3","azul",245,true);
		
		Cliente cliente1=new Cliente("38076977","alexander","zurita");
		
		AutoAlquilado alquiler1=new AutoAlquilado(auto1,cliente1,21,11);
		
		
		System.out.println("Auto alquilado");
		
		System.out.println("Cliente : "+alquiler1.getCliente().getdni()+"  " +
		alquiler1.getCliente().getNombre() +"  "+
		alquiler1.getCliente().getApellido());
		
		System.out.println("Auto : "+alquiler1.getAuto().getmatricula());
		
		
		Auto Auto2=new Auto("QWE123","citroen","c3","azul",135,true);
		Turismo turismo1=new Turismo("43TURISMO","maza","flash","rojo",550,true,2,false);
		Deportivo deportivo1=new Deportivo("12DEPORTIVO","BMW","SLIM","amarillo",345,true,120);
		Furgoneta furgoneta1=new Furgoneta("67FURGONETA","Ford","transit","blanco",250,true,500,450);
		
		System.out.println("Auto2 : "+Auto2.getAtributos());
		System.out.println(" ");
		System.out.println("Turismo1 : "+turismo1.getAtributos());
		System.out.println(" ");
		System.out.println("Deportivo : "+deportivo1.getAtributos());
		System.out.println(" ");
		System.out.println("Furgoneta : "+furgoneta1.getAtributos());
		
		
		
		Auto autodeprueba=new Turismo("43TURISMO","maza","flash","rojo",550,true,2,false);
		
		Turismo nuevoDePrueba= (Turismo) autodeprueba;
	}*/

	

	private Auto auto;
	private Cliente cliente;
	
	private int diaAlquiler;

	
	
	//CONSTRUCTORES
	public AutoAlquilado(Auto auto, Cliente cliente, int diaAlquiler) {

		this.auto = auto;
		this.cliente = cliente;
		this.diaAlquiler = diaAlquiler;

	}
	
	
	
	//GETTERS Y SETTERS
	public Cliente getCliente() {
		return this.cliente;
	}
	public Auto getAuto() {
		return this.auto;
	}


	public int getDiaAlquiler() {
		return diaAlquiler;
	}
	

}
