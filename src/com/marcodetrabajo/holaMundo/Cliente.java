package com.marcodetrabajo.holaMundo;

public class Cliente {
	
	
	private String dni;
	private String nombre;
	private String apellido;
	

	//CONSTRUCTOR
	public Cliente(String dni, String nombre, String apellido) {

		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
	}
	
	
//GETTERS y SETTERS
	
	public String getdni() {
		return this.dni;
	}
	public String getnombre() {
		return this.nombre;
	}
	public String getapellido() {
		return this.apellido;
	}

	
	public String getAtributos(){
		
		return
				"DNI : " + getdni()+" "+
				"nombre : " + getnombre()+" "+
				"apellido : " + getapellido();
	}

	
	
}
