package com.marcodetrabajo.holaMundo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EmpresaAlquilerAutos {



	
	private String dni;
	private String nombre;
	private String paginaWeb;
	
	
	private int totalClientes;
	private List<Cliente> clientes;
	
	
	private int totalAutos;
	private List<Auto> autos;
	
	
	private int totalAlquilados;
	private List<AutoAlquilado> alquileres;
	
	
	
	//GETTERS y SETTERS

	public String getdni() {
		return dni;
	}
	public String getNombre() {
		return nombre;
	}
	public String getPaginaWeb() {
		return paginaWeb;
	}
	public int getTotalClientes() {
		return totalClientes;
	}
	public int getTotalAutos() {
		return totalAutos;
	}
	public int getTotalAlquilados() {
		return totalAlquilados;
	}
	





	//METODO CONSTRUCTOR
	public EmpresaAlquilerAutos(String dni, String nombre, String paginaWeb) {
		
		this.dni=dni;
		this.nombre=nombre;
		this.paginaWeb=paginaWeb;
		
		this.totalClientes=0;
		this.clientes=new ArrayList<Cliente>();
		
		this.totalAutos=0;
		this.autos=new ArrayList<Auto>();
		
		this.totalAlquilados=0;
		this.alquileres=new ArrayList<AutoAlquilado>();
	
	}
	
	
	
	
	
	//METODOS PARA REGISTRAR AUTOS O CLIENTES NUEVOS
	
	public void registrarCliente(Cliente cliente) {
		
		this.clientes.add(cliente);
		
		this.totalClientes++;
		
	}
	
	public void registrarAuto(Auto auto) {
		this.autos.add(auto);
		
		this.totalAutos++;
	}
	
	
	//METODOS PARA IMPRIMIR INFORMES
	
	public void imprimirCliente() {
		System.out.println("DNI cliente\tNombre\n");
		
		for(int i=0;i<this.totalClientes;i++) 

			System.out.println((String)clientes.get(i).getAtributos());
	
	}
	
	public void imprimirAuto() {
		System.out.println("matricula\tModelo "+"\tImporte Disponible\n");
		
		for (Auto a : this.autos)
				System.out.println((String)a.getAtributos());
			
		
		
	}
	
	
//PARA BUSCAR CLIENTE Y AUTO CREAMOS GETCLIENTE Y GETAUTO
	//LAS DOS FUNCIONES VAN A SER PRIVADAS PORQUE LUEGO LAS USAMOS EN OTRA FUNCION
	//LA FUNCION QUE REUNE ESTAS DOS FUNCIONES VA A SER "ALQUILARAUTO" QUE ESTA SI SE VA
	//A PODER CONSULTAR Y POR ELLO VA A SER PUBLICA:
	
	private Cliente getCliente(String dni) {

		for (int i=0;i<this.getTotalClientes();i++)

			if((String)clientes.get(i).getdni()==dni)
				return (Cliente)clientes.get(i);
		
		return null;			
	}
	
	private Auto getAuto(String matricula) {
		
		for (int i=0;i<getTotalAutos();i++) 
		
			if ((String)autos.get(i).getmatricula()==matricula)
				return (Auto)autos.get(i);
			
		return null;	
	}


	
	public void alquilarAuto(String matricula,String dni,int dias) {
		
		Cliente cliente=getCliente(dni);
		Auto auto=getAuto(matricula);

		
		
		if (auto.getdisponibilidad()) {
			auto.setdisponibilidad(false);
			
			alquileres.add(new AutoAlquilado(auto, cliente, dias));
			
			this.totalAlquilados++;
			
		}
		
	}
	

	public void recibirAuto(String matricula) {
		
		Auto auto=getAuto(matricula);
		
		if (auto!=null)
			auto.setdisponibilidad(true);
	}
	
	Scanner entradaTeclado=new Scanner(System.in);
	

		
	public void autoPorTeclado() {
		
		System.out.println("----- INGRESE LOS SIGUIENTES DATOS ----- ");
		System.out.println("Matricula: ");
		String matricula=entradaTeclado.nextLine();
		
		System.out.println("Marca: ");
		String marca=entradaTeclado.nextLine();
		
		System.out.println("Modelo: ");
		String modelo=entradaTeclado.nextLine();
		
		System.out.println("Color: ");
		String color=entradaTeclado.nextLine();
		
		System.out.println("Tarifa: ");
		int tarifa=entradaTeclado.nextInt();
		
		
		System.out.println("Ingrese tipo de vehiculo: \n 1-Turismo \n 2-Deportivo \n 3-Furgoneta");
		
		int tipoVehiculo=entradaTeclado.nextInt();
		
		
		//String matricula, String marca, String modelo, String color, float tarifa,      boolean disponibilidad,int numPuertas,boolean marchaAutomatica
		switch (tipoVehiculo) {
		
		
		case 1: 
			
			System.out.println("numero de puertas?");
			int numPuertas=entradaTeclado.nextInt();
			
			System.out.println("tiene marcha automatica?: ");
			String marchaAut=(entradaTeclado.next());
			
			
			boolean marchaAuto=true;
			
			
			if(marchaAut.equals("si")||marchaAut.equals("SI")||marchaAut.equals("Si")||marchaAut.equals("sI")) {
				marchaAuto = true;}
			else {
				marchaAuto = false;}
			
			boolean disponibilidad=true;
			
			registrarAuto(new Turismo(matricula,marca,modelo,color,tarifa,disponibilidad,numPuertas,marchaAuto));
		
			
		break;

		default:
			break;
		}
	}

	
}
